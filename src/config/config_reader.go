package config

import (
	"io"
	"os"

	"gopkg.in/yaml.v2"
)

func LoadConfigFromFile(configPath string) (Config, error) {
	file := defaultConfig()
	yamlFile, err := os.Open(configPath)
	if err != nil {
		return file, err
	}

	byteValue, err := io.ReadAll(yamlFile)
	if err != nil {
		return file, err
	}

	err = yaml.UnmarshalStrict(byteValue, &file)
	if err != nil {
		return file, err
	}

	return file, nil
}

func defaultConfig() Config {
	return Config{
		Server: ServerConfig{
			Port: 8000,
		},
	}
}
