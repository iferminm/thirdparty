package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Routes struct {
	Namespace *string
	Routes    []Route
}

func (r Routes) Print() {
	for _, v := range r.Routes {
		v.Print()
	}
}

type Route struct {
	Path  string
	Rules []Rule
}

func (r Route) Print() {
	fmt.Printf("Path: %s\n", r.Path)
	for _, v := range r.Rules {
		v.Print()
	}
}

type Rule struct {
	Method  string
	Headers *[]YamlStringMap
	Args    *[]YamlStringMap
	Body    *string
	Wait    *string
	Response
}

func (r Rule) GetWaitTime() (time.Duration, error) {
	defaultWait := time.Duration(0)
	if r.Wait == nil {
		return defaultWait, nil
	}
	matcher := regexp.MustCompile(`(?P<wait>\d+)(?P<unit>[smh])$`)
	matches := matcher.FindStringSubmatch(*r.Wait)
	if len(matches) < 3 {
		return defaultWait, fmt.Errorf("Error: response waiting time is not valid %s", *r.Wait)
	}

	timeInt, _ := strconv.Atoi(matches[1])
	waitDuration := time.Duration(timeInt)

	var wait time.Duration
	switch unit := matches[2]; unit {
	case "s":
		wait = waitDuration * time.Second
	case "m":
		wait = waitDuration * time.Minute
	case "h":
		wait = waitDuration * time.Hour
	}
	return wait, nil
}

func (r Rule) Verify(req *http.Request) bool {
	if strings.ToLower(req.Method) != strings.ToLower(r.Method) {
		return false
	}
	if r.Headers != nil {
		for _, header := range *r.Headers {
			if req.Header.Get(header.Name) != header.Value {
				return false
			}
		}
	}

	if strings.ToLower(req.Method) == "get" {
		if r.Args != nil {
			for _, arg := range *r.Args {
				if req.URL.Query().Get(arg.Name) != arg.Value {
					return false
				}
			}
		}
	} else if strings.ToLower(req.Method) == "post" {
		if r.Body != nil {
			var dBody map[string]interface{}
			json.Unmarshal([]byte(*r.Body), &dBody)

			reqRawBody, _ := ioutil.ReadAll(req.Body)
			var dReqBody map[string]interface{}
			json.Unmarshal(reqRawBody, &dReqBody)

			for k, v := range dBody {
				rvalue, ok := dReqBody[k]
				if !ok || rvalue != v {
					return false
				}
			}
		}
	}
	return true
}

func (r Rule) Print() {
	fmt.Printf("Method: %s\n", r.Method)
	if r.Headers != nil {
		fmt.Println("Headers:")
		for _, v := range *r.Headers {
			v.Print()
		}
	}
	if r.Args != nil {
		fmt.Println("Args:")
		for _, v := range *r.Args {
			v.Print()
		}
	}
	if r.Body != nil {
		fmt.Printf("Body: %s\n", *r.Body)
	}
	r.Response.Print()
}

type Response struct {
	Status  int
	Headers *[]YamlStringMap
	Body    *string
}

func (r Response) Print() {
	fmt.Printf("Status: %d\n", r.Status)
	if r.Headers != nil {
		for _, v := range *r.Headers {
			v.Print()
		}
	}
	if r.Body != nil {
		fmt.Printf("Body: %s\n", *r.Body)
	}
}

type YamlStringMap struct {
	Name  string
	Value string
}

func (y YamlStringMap) Print() {
	fmt.Printf("name: %s\n", y.Name)
	fmt.Printf("value: %s\n", y.Value)
}
