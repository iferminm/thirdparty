package routes

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

func LoadRoutesFromDir(routesDirPath string) ([]*Routes, error) {
	files, err := getFilesFromDirectory(routesDirPath)
	if err != nil {
		return nil, err
	}
	result := make([]*Routes, 5)
	for _, file := range files {
		routes, err := LoadRoutesFromFile(file.Name())
		if err != nil {
			return nil, err
		}
		result = append(result, routes)
	}
	return result, nil
}

func getFilesFromDirectory(routesDirPath string) ([]os.FileInfo, error) {
	files, err := ioutil.ReadDir(routesDirPath)
	if err != nil {
		return nil, fmt.Errorf("error loading the routes from the file: %v", err)
	}
	return files, nil
}

func LoadRoutesFromFile(routesPath string) (*Routes, error) {
	file, err := os.Open(routesPath)
	if err != nil {
		return nil, err
	}

	byteValue, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	routes := Routes{}
	err = yaml.UnmarshalStrict(byteValue, &routes)
	if err != nil {
		return nil, err
	}

	return &routes, nil
}
