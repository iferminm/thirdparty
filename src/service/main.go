package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/iferminm/thirdparty.git/src/config"
	"gitlab.com/iferminm/thirdparty.git/src/routes"
)

func main() {
	pathToConfig := "config/config.yml"
	pathToRoutes := "routes/routes.yml"

	serviceConfig, err := config.LoadConfigFromFile(pathToConfig)
	if err != nil {
		panic(err)
	}

	routesFromFile, err := routes.LoadRoutesFromFile(pathToRoutes)
	if err != nil {
		panic(err)
	}

	setupAndRunServer(serviceConfig, routesFromFile)
}

func setupAndRunServer(config config.Config, routes *routes.Routes) {
	for _, route := range routes.Routes {
		http.HandleFunc(route.Path, generateController(route.Rules))
	}

	serverUrl := fmt.Sprintf("localhost:%d", config.Server.Port)
	fmt.Printf("Server is running at %s...\n", serverUrl)
	http.ListenAndServe(serverUrl, nil)
}

func generateController(rules []routes.Rule) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var targetRule routes.Rule

		for _, rule := range rules {
			if found := rule.Verify(r); found {
				targetRule = rule
			}
		}

		if targetRule.Response.Headers != nil {
			for _, hdr := range *targetRule.Response.Headers {
				w.Header().Add(hdr.Name, hdr.Value)
			}
		}
		w.WriteHeader(targetRule.Response.Status)
		wait, err := targetRule.GetWaitTime()
		if err != nil {
			fmt.Println("Error: %w", err)
		}
		time.Sleep(wait)
		w.Write([]byte(*targetRule.Response.Body))
	}
}
